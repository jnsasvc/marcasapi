<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('users')->insert(
            [
                'name'=> 'testUser',
                'email' => 'user@test2.com',
                'password'=> Hash::make('ready4sk8'),
                'email_verified_at' => '2020-08-21 15:52:34',
                'created_at'=> '2020-08-21 15:52:34',
                'updated_at'=> '2020-08-21 15:52:34',
            ]
        );
    }
}
