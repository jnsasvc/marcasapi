<?php

use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert(
            [
                'name'=> 'Mercedes',
                'created_at'=> '2020-08-21 15:52:34',
                'updated_at'=> '2020-08-21 15:52:34',
            ]
        );
        
        DB::table('brands')->insert(
            [
                'name'=> 'Dodge',
                'created_at'=> '2020-08-21 15:52:34',
                'updated_at'=> '2020-08-21 15:52:34',
            ]
        );
        DB::table('brands')->insert(
            [
                'name'=> 'Jeep',
                'created_at'=> '2020-08-21 15:52:34',
                'updated_at'=> '2020-08-21 15:52:34',
            ]
        );
        DB::table('brands')->insert(
            [
                'name'=> 'Ford',
                'created_at'=> '2020-08-21 15:52:34',
                'updated_at'=> '2020-08-21 15:52:34',
            ]
        );
    }
}
