<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{

    public function login(Request $request)
    {
        $user = User::whereEmail($request->email)->first();
        if($user && Hash::check($request->password, $user->password)){
            $user->api_token = Str::random(60);
            $user->save();
            return response()->json([
                'token' =>$user->api_token
            ]);
        }else{
            return response()->json('Unauthorized',401);
        }
    }

    public function logout()
    {
        $user = auth()->user();
        $user->api_token = null;
        $user->save();
    }


}