<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index()
    {
        return Brand::all();
    }

    public function show($id)
    {
        return Brand::findOrFail($id);
    }

    public function store(Request $request){
        $this->validate($request,$this->brandrules);
         $brand =Brand::create($request->all());
         return response()->json($brand,201);
    }

    public function update($id,Request $request)
    {
        $this->validate($request,$this->brandrules);
        $brand= Brand::find($id);
        $brand->update($request->all());

        return $brand;
    }

    public function destroy($id)
    {
       return Brand::destroy($id);
    }

    protected $brandrules = [
        'name' => 'required|min:4'
    ];
}
