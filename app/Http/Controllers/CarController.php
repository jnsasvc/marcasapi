<?php

namespace App\Http\Controllers;

use App\Models\Car;
use Illuminate\Http\Request;

class CarController extends Controller
{
    public function index($id)
    {
        return Car::where('brand_id',$id)->get();
    }

    public function show($id)
    {
        return Car::findOrFail($id);
    }

    public function store(Request $request){
        $this->validate($request,$this->carrules);
         $car =Car::create($request->all());
         return response()->json($car,201);
    }

    public function update($id,Request $request)
    {
        $this->validate($request,$this->carrules);
        $car= Car::find($id);
        $car->update($request->all());

        return $car;
    }

    public function destroy($id)
    {
       return Car::destroy($id);
    }

    protected $carrules = [
        'name' => 'required|min:4',
        'brand_id'=> 'required'
    ];
}
