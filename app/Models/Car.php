<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Car extends Model 
{

    protected $fillable = [
        'name','brand_id'
    ];

    protected $hidden = [
        'created_at','updated_at'
    ];
}
