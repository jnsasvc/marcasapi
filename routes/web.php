<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['prefix' => 'api'], function () use ($router) {
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->get('brands',['as'=> 'brands','uses'=>'BrandController@index']);
        $router->get('brands/{id}',['as'=> 'brands.show','uses'=>'BrandController@show']);
        $router->post('brands',['as'=> 'brands.store','uses'=>'BrandController@store']);
        $router->put('brands/{id}',['as'=> 'brands.update','uses'=>'BrandController@update']);
        $router->delete('brands/{id}',['as'=> 'brands.delete','uses'=>'BrandController@destroy']);

        $router->get('carlist/{id}',['as'=> 'cars','uses'=>'CarController@index']);
        $router->get('cars/{id}',['as'=> 'cars.show','uses'=>'CarController@show']);
        $router->post('cars',['as'=> 'cars.store','uses'=>'CarController@store']);
        $router->put('cars/{id}',['as'=> 'cars.update','uses'=>'CarController@update']);
        $router->delete('cars/{id}',['as'=> 'cars.delete','uses'=>'CarController@destroy']);
    });
    $router->get('login',['as'=> 'login','uses'=>'UserController@login']);
    $router->get('logout',['as'=> 'logout','uses'=>'UserController@logout']);
});
